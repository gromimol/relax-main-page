$(document).ready(function (){

    $('.slider').slick({
        slidesToShow: 1,
        prevArrow: '<span class="prev-arrow"><svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10.1309 18.3914L1.78303 10.0435L10.1309 1.69571" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></span>',
        nextArrow: '<span class="next-arrow"><svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.86914 18.3914L10.217 10.0435L1.86914 1.69571" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></span>',
        responsive: [
            {
                breakpoint: 767,
                settings: 'unslick'
            }
        ]
    })
    // fake select https://codepen.io/l4ci/pen/gPjYma?limit=all&page=2&q=select
    $('.select').on('click','.placeholder',function(){
        const parent = $(this).closest('.select');
        if ( ! parent.hasClass('is-open')){
            parent.addClass('is-open');
            $('.select.is-open').not(parent).removeClass('is-open');
        }else{
            parent.removeClass('is-open');
        }
    }).on('click','ul>li',function(){
        const parent = $(this).closest('.select');
        const tab = $(this).data('tab')
        $(this).closest('.for-partners').find('.slider').removeClass('active')
        $(`#${tab}`).addClass('active')
        $(this).addClass('active').siblings('li').removeClass('active')
        parent.removeClass('is-open').find('.placeholder').text( $(this).text().toLowerCase() );
    });
    // tabs
    $('.tab-list').on('click', 'li', function () {
        const tabId = $(this).data('tab')
        $(this).addClass('active').siblings('li').removeClass('active')
        $(this).closest('.tabs-wrapper').find('.tab-content').removeClass('active')
        $(`#${tabId}`).addClass('active')
    })

    // scroll to anchor
    $('.js--scrollTo').on('click', function(e) {
        e.preventDefault()
        let href = $(this).attr('href');

        $('html, body').animate({
            scrollTop: $(href).offset().top
        }, {
            duration: 370,   // по умолчанию «400»
        });
    });

    // slider for adv banners
    $('.banner-slider__main').each(function () {
        $(this).slick({
            slidesToShow: 1,
            asNavFor: $(this).parent('.banner-slider-block').find('.banner-slider__descr'),
            dots: true,
            arrows: false,
            appendDots: $(this).parent('.banner-slider-block').find('.banner-slider-counter'),
            fade: true
        })
        $('.banner-slider__descr').slick({
            slidesToShow: 1,
            asNavFor: $(this).parent('.banner-slider-block').find('.banner-slider__main'),
            fade: true,
            arrows: false,
        })
    })

})
